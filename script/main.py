class GitlabTraining:

    def __init__(self, date, content):
        self.date = date
        self.content = content

    def get_content(self):
        return self.content

    def get_date(self):
        return self.date