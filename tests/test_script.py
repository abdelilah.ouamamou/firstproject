import pytest
from script.main import GitlabTraining

def test_date():
    response  = GitlabTraining(
        date="2023-12-12",
        content="gitlab training test")

    date = response.get_date()
    assert date == "2023-12-12"

def test_content():
    obj = GitlabTraining(
        date="2022-1-10",
        content="test training"
    )
    content = obj.get_content()
    assert content == "test training"